<?php
// $Id$

/**
 * @file
 * Menu path for admin pages.
 */

/**
 * Implements hook_menu().
 */
function _affiliate_store_admin_menu(&$items) {
  $items['admin/settings/affiliate-store'] = array(
    'title' => 'Affiliate store',
    'description' => 'Configure how affiliate store operates.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('affiliate_store_settings_page'),
    'access callback' => 'user_access',
    'access arguments' => array('administer affiliate store'),
    'file' => 'includes/admin.inc',
  );
}
