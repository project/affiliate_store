<?php
// $Id$

/**
 * @file
 * Set up web store for selling products from merchants via affiliate
 * relationship.
 */

/**
 * Implements hook_menu().
 */
function affiliate_store_menu() {
  $menu = array();
  _affiliate_store_passthrough($menu, 'menu');
  return $menu;
}

/**
 * Implements hook_help().
 */
function affiliate_store_help($path) {
  switch ($path) {
    case 'admin/help#affiliate_store':
      // Return a line-break version of the README file.
      return filter_filter(
        'process', 2, NULL, file_get_contents(dirname(__FILE__) . '/README.txt')
      );
  }
}

/**
 * Implements hook_perm().
 */
function affiliate_store_perm() {
  return array(
    // Administrator has all permissions within affiliate_store module.
    'administer affiliate store',
  );
}

/**
 * Provide a hook passthrough to included files.
 *
 * To organize things neatly, each tool gets its own toolname.$type.inc
 * file. If it exists, it's loaded and affiliate_hub_$tool_$type() is executed.
 * Code copy from CTools.
 *
 * @param &$items
 *   An array of mixed items to pass through hooks and collect return values.
 * @param $type
 *   Type of hook to invoke.
 * @param $dir
 *   Directory to search for include files.
 *
 * @return
 *   An array of populated items according to type of hook.
 */
function _affiliate_store_passthrough(&$items, $type = 'theme',
$dir = 'includes') {
  $files = drupal_system_listing(
    ".$type.inc$",
    drupal_get_path('module', 'affiliate_store') . "/$dir",
    'name', 0
  );
  foreach ($files as $file) {
    include_once "./$file->filename";
    list($tool) = explode('.', $file->name, 2);

    $function = '_affiliate_store_' . str_replace('-', '_', $tool) . "_$type";
    if (function_exists($function)) {
      $function($items);
    }
  }
}
