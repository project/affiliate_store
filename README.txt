# $Id$

Affiliate Store
---------------

This module sets up a web store in your website for selling products from 
merchants which you have an affiliate relationship with.
